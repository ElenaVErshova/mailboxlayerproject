import { describe, expect, it } from '@jest/globals';
import { apiProvider } from '../framework/index';
import { email, testTable } from '../framework/data/testData';
import { errors } from '../framework/config/errors';
import { ParamsBuilder } from '../framework/builder/paramsBuilder';

describe('Тесты на сервис валидации почтовых ящиков', () => {
  // happypath
  it('Проверяем валидный вариант email', async () => {
    const params = new ParamsBuilder()
      .addAccessKey()
      .addEmail(email.goodMail)
      .buildParamsTail();

    const response = await apiProvider().checkMail().check(params);
    expect(response.status).toBe(200);
    const responseJson = await response.json();
    expect(responseJson.email).toEqual(email.goodMail);
    expect(responseJson.did_you_mean).toEqual(email.suggestedGoodMail);
    expect(responseJson.user).toEqual(email.goodMail.split('@')[0]);
    expect(responseJson.domain).toEqual(email.goodMail.split('@')[1]);
    expect(responseJson.smtp_check).toBeTruthy();
    expect(responseJson.score).toBeGreaterThan(0);
    expect(responseJson.score).toBeLessThan(1);
  });
  it('Проверяем валидный вариант email c опциональными параметрами', async () => {
    const params = new ParamsBuilder()
      .addAccessKey()
      .addEmail(email.goodMail)
      .addSmtp(1)
      .addFormat(1)
      .buildParamsTail();
    const response = await apiProvider().checkMail().check(params);
    expect(response.status).toBe(200);
    const responseJson = await response.json();
    expect(responseJson.email).toEqual(email.goodMail);
    expect(responseJson.did_you_mean).toEqual(email.suggestedGoodMail);
    expect(responseJson.user).toEqual(email.goodMail.split('@')[0]);
    expect(responseJson.domain).toEqual(email.goodMail.split('@')[1]);
    expect(responseJson.smtp_check).toBeTruthy();
    expect(responseJson.score).toBeGreaterThan(0);
    expect(responseJson.score).toBeLessThan(1);
  });
  // parametrizied
  it.each(testTable)('Проверка %s', async (testEmail, expected, format) => {
    const params = new ParamsBuilder()
      .addAccessKey()
      .addEmail(testEmail)
      .buildParamsTail();

    const response = await apiProvider().checkMail().check(params);
    expect(response.status).toBe(200);
    const responseJson = await response.json();
    expect(responseJson.email).toEqual(expected);
    expect(responseJson.format_valid).toEqual(format);
  });
  // no api_key
  it('Проверка запроса без api_key', async () => {
    const params = new ParamsBuilder().addEmail(email.goodMail).buildParamsTail();

    const response = await apiProvider().checkMail().check(params);
    expect(response.status).toBe(200);
    const responseJson = await response.json();
    expect(responseJson.success).toBeFalsy();
    expect(responseJson.error).toEqual(errors.noAccessKey);
  });
});
