import fetch from 'node-fetch';
import { urls } from '../config/index';

const CheckMail = function CheckMail() {
  this.check = async (params) => {
    const response = await fetch(`${urls.baseUrl}?${params}`);
    return response;
  };
};

export { CheckMail };
