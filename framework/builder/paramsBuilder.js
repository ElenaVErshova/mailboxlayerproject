import { accessKey } from '../config/params';

export { urls } from '../config/index';

const ParamsBuilder = function ParamsBuilder() {
  this.addAccessKey = () => {
    this.accessKey = `access_key=${accessKey.goodKey}`;
    return this;
  };
  this.addEmail = (email) => {
    this.email = `email=${email}`;
    return this;
  };
  // default = 1
  this.addSmtp = (smtpCheck) => {
    if (smtpCheck === 0 || smtpCheck === 1) {
      this.smtp = `smtp=${smtpCheck}`;
    }
    return this;
  };
  // default = 0, use 1 for debug only
  this.addFormat = (formatJson) => {
    if (formatJson === 0 || formatJson === 1) {
      this.format = `format=${formatJson}`;
    }
    return this;
  };
  // JSONP callback
  this.addCallback = () => {
    this.callback = 'callback=CALLBACK_FUNCTION';
    return this;
  };
  this.buildParamsTail = () => {
    let urlTail = '';
    Object.values(this)
      .filter((item) => typeof item !== 'function')
      .forEach((item) => {
        urlTail = `${urlTail + item}&`;
      });

    return urlTail === '' ? urlTail : urlTail.substring(0, urlTail.length - 1);
  };
};

export { ParamsBuilder };
