const email = {
  goodMail: 'testotus1@mail.ru',
  suggestedGoodMail: 'testotus1@mail.com',
  fakeEmail: 'fakeqwerty@mail.ru',
  noAtSymbol: 'qwerty.ru',
  wrongDomain: 'qwerty@asdfgh',
  noName: '@mail.ru',
  noDomain: 'qwerty@',
};

const testTable = [
  [email.fakeEmail, email.fakeEmail, true],
  [email.noAtSymbol, email.noAtSymbol, false],
  [email.wrongDomain, email.wrongDomain, false],
  [email.noName, email.noName, false],
  [email.noDomain, email.noDomain, false],
];

export { email, testTable };
