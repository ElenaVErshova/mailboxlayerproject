import { CheckMail } from './controllers/index';

const apiProvider = () => ({
  checkMail: () => new CheckMail(),
});

export { apiProvider };
